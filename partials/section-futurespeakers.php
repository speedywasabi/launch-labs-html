<section id="future-speakers">
    <header>
        Future Speakers
        <?php include 'partials/navbar-toggle.php'; ?>
    </header>
    <h2><time datetime="2015-05-25 18:30">29th May, 2015</time></h2>
    <p class="text-muted"><i>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</i></p>
    <div class="row">
        <div class="photo col-xs-4 col-sm-2 col-md-4 col-lg-3">
            <img src="/assets/img/thumb-lawrence.png" alt="Lawrence Holmes" class="img-responsive">
        </div>
        <div class="col-xs-8 col-sm-10 col-md-8 col-lg-9">
            <h3>Lawrence Holmes</h3>
            <h4 class="text-muted"><i>Co-founder of NewCo</i></h4>
            <p class="visible-md-block visible-lg-block">Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nulla vitae elit libero, a pharetra augue. Maecenas faucibus mollis interdum. </p>
        </div>
    </div>
    <div class="row">
        <div class="photo col-xs-4 col-sm-2 col-md-4 col-lg-3">
            <img src="/assets/img/thumb-phillip.png" alt="Phillip Ross" class="img-responsive">
        </div>
        <div class="col-xs-8 col-sm-10 col-md-8 col-lg-9">
            <h3>Phillip Ross</h3>
            <h4 class="text-muted"><i>CEO at Digital Place</i></h4>
            <p class="visible-md-block visible-lg-block">Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nulla vitae elit libero, a pharetra augue. Maecenas faucibus mollis interdum. </p>

        </div>
    </div>

    <h2><time datetime="2015-05-25 18:30">29th May, 2015</time></h2>
    <p class="text-muted"><i>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</i></p>
    <div class="row">
        <div class="photo col-xs-4 col-sm-2 col-md-4 col-lg-3">
            <img src="/assets/img/thumb-brandon.png" alt="Brandon Jones" class="img-responsive">
        </div>
        <div class="col-xs-8 col-sm-10 col-md-8 col-lg-9">
            <h3>Brandon Jones</h3>
            <h4 class="text-muted"><i>Co-founder of AppzNStuf</i></h4>
            <p class="visible-md-block visible-lg-block">Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nulla vitae elit libero, a pharetra augue. Maecenas faucibus mollis interdum. </p>
        </div>
    </div>

    <?php include 'partials/subscribe.php'; ?>
</section>