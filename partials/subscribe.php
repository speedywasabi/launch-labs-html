<form data-parsley-validate class="subscribe">
    <label for="subscribe"><span class="hidden-xs hidden-sm extra-text">Can't make the next one?</span> Let us notify you of future events</label>
    <div class="form-wrap">
    	<input type="email" class="form-control" required id="subscribe" placeholder="Email address">
    	<input type="submit" class="btn btn-default" value="Subscribe">
    </div>
</form>