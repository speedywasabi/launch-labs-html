<section id="next-event">
    <header>Next Event
        <?php include 'partials/navbar-toggle.php'; ?>
    </header>
    <h2><time datetime="2015-05-19 18:30">Tue 19th May, 2015</time></h2>
    <h1>Want to learn how a garbage man became the founder of a successful software agency?</h1>
    <p>Aenean lacinia bibendum nulla sed consectetur. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Fusce dapibus, tellus ac cursu:</p>
    <ul>
        <li>Ipsum Fusce Ligula Tortor</li>
        <li>Magna Consectetur Aenean</li>
        <li>Bibendum Tortor Pellentesque Vehicula</li>
    </ul>
    <div class="row">
        <div class="col-sm-6 col-md-12 col-lg-6">
            <h3>Lawrence Holmes</h3>
            <h4 class="text-muted"><i>Co-founder of NewCo</i></h4>
            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nulla vitae elit libero, a pharetra augue. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. </p>
            <p>Maecenas faucibus mollis <a href="#">interdum.com.au</a> </p>
        </div>
        <div class="col-sm-6 col-md-12 col-lg-6">
            <img src="/assets/img/img-lawrence.jpg" alt="Lawrence Holmes" class="img-responsive">
        </div>
    </div>
    <table class="table">
        <tr><th>6.30pm</th><td>Doors open</td></tr>
        <tr><th>7.00pm</th><td>Meet and greet</td></tr>
        <tr><th>8.00pm</th><td>Presentation begins</td></tr>
        <tr><th>9.00pm</th><td>Finish up for the night</td></tr>
    </table> 
    <ul class="details">
        <li>
            <span class="icon-time"></span><b>Tue 19th May, 2015 <br class="hidden-md hidden-lg">
            <a href="/ical/event.ics" class="add-to-cal">Add to my calendar</a></b>
        </li>
        <li>
            <span class="icon-place"></span><b>68 Greville st, Prahran, 3181</b>
        </li>
    </ul>
    <div id="map-canvas"></div>
</section>