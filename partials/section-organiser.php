<section id="organiser">
    <header>
        About the Organiser
        <?php include 'partials/navbar-toggle.php'; ?>
    </header>
    <h2>Bravo!</h2>
    <p>Anean lacinia bibendum nulla sed consectetur. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Fusce dapibus, tellus ac cursu:</p>

    <ul>
        <li>Ipsum Fusce Ligula Tortor</li>
        <li>Magna Consectetur Aenean</li>
        <li>Bibendum Tortor Pellentesque Vehicula</li>
    </ul>
    <p><a href="http://saybravo.com.au" target="_blank">saybravo.com.au</a></p>
    <br><br>
    <img src="/assets/img/launchlab-logo-sml.svg" width="50" alt="Launch Lab Logo">
</section>