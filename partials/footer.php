<footer>
    <?php include 'partials/subscribe.php'; ?>
    <div class="row">
    	<div class="col-xs-7 small">
    		&copy; 2015 Launch Labs •  Bravo
    	</div>
    	<div class="col-xs-5 text-right">
		    <ul class="social">
		        <li><a href="#" target="_blank"><span class="icon-facebook"></span><i class="sr-only">Facebook</i></a></li>
		        <li><a href="#" target="_blank"><span class="icon-twitter"></span><i class="sr-only">Twitter</i></a></li>
		        <li><a href="#" target="_blank"><span class="icon-instagram"></span><i class="sr-only">Instagram</i></a></li>
		    </ul>
		</div>
	</div>
</footer>