<section id="sponsors">
    <header>
    	Our Sponsors
    	<?php include 'partials/navbar-toggle.php'; ?>
    </header>
    <h2 class="visible-md-block visible-lg-block">Another Line Here</h2>
    <ul class="logos">
        <li><a href="#"><img src="/assets/img/sponsor-portphillip.jpg" alt="City of Port Phillip"></a></li>
        <li><a href="#"><img src="/assets/img/sponsor-stonnington.jpg" alt="City of Stonington"></a></li>
        <li><a href="#"><img src="/assets/img/sponsor-mint.jpg" alt="Min"></a></li>
        <li><a href="#"><img src="/assets/img/sponsor-taurocapital.jpg" alt="Tauro Capital Partners"></a></li>
    </ul>
</section>