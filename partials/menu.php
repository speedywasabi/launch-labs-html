<nav class="nav-block">
	<h1 class="logo">Launch Labs by Bravo</h1>
	<ul class="menu nav" role="tablist">
		<li><a href="#about-us">About Us</a></li>
		<li><a href="#next-event">Next Event</a></li>
		<li><a href="#rsvp">Reserve a Seat</a></li>
		<li><a href="#future-speakers">Future Speakers</a></li>
		<li><a href="#sponsors">Our Sponsors</a></li>
		<li><a href="#organiser">About the Organiser</a></li>
	</ul>

	<?php include 'partials/footer.php'; ?>
</nav>