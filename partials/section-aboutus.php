<section id="about-us">
	<div class="inner">
	    <h1 class="logo">LaunchLabs</h1>
	    <h2>Launch Labs are free talks aimed to <span>educate</span> and <span>inspire</span> startups and entrepreneurs in Melbourne.</h2>
    	<a href="#rsvp" class="btn btn-info">Reserve your free seat to the next event</a>
	</div>
</section>