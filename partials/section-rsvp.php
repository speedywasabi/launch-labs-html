<section id="rsvp">
    <header>
        RSVP for the Next Event
        <?php include 'partials/navbar-toggle.php'; ?>
    </header>
    <h2>Come along and be inspired. For free!</h2>
    <p class="text-muted"><i>Please make sure you RSVP to help us anticipate numbers and so we can notify you of any speaker changes, cancellations, changes in venue or updates to the event.</i></p>
    <form data-parsley-validate>
        <div class="form-group">
            <label for="rsvp-name">Name</label>
            <input type="text" class="form-control" id="rsvp-name" required>
        </div>
        <div class="form-group">
            <label for="rsvp-email">Email address</label>
            <input type="email" class="form-control" id="rsvp-email" required>
        </div>
        <div class="form-group">
            <label for="rsvp-phone">Phone Number</label>
            <input type="text" class="form-control" id="rsvp-phone" required>
        </div>
        <div class="form-group">
            <label for="rsvp-msg">Tell us a bit about yourself and what you do</label>
            <textarea  rows="3" class="form-control" id="rsvp-msg" required></textarea>
        </div>
        <div class="checkbox">
            <input id="rsvp-optin" type="checkbox"> <label for="rsvp-optin">Notify me of future events</label>
        </div>
        <input type="submit" value="Reserve my seat" class="btn btn-primary">
    </form>
</section>