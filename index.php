<!doctype html>
<html class="no-js" lang="EN">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Launch Labs - by Bravo</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="/assets/img/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="/assets/img/favicon.png">

        <!--Stylesheet-->
        <link rel="stylesheet" href="/assets/css/launchlabs.css">

        <!--Fonts-->
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css'>

        <!--[if lt IE 9]>
            <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
        <![endif]-->
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <?php include 'partials/menu.php'; ?>

        <div class="content">
            <?php include 'partials/section-aboutus.php'; ?>
            <?php include 'partials/section-nextevent.php'; ?>
            <?php include 'partials/section-rsvp.php'; ?>
            <?php include 'partials/section-futurespeakers.php'; ?>
            <?php include 'partials/section-sponsors.php'; ?>
            <?php include 'partials/section-organiser.php'; ?>
        </div>
        <?php include 'partials/scripts.php'; ?>
    </body>

</html>