// Google map
function initGoogleMap() {
	var mapCanvas = document.getElementById('map-canvas');
	var mapOptions = {
	  center: new google.maps.LatLng(-37.848978, 144.989035),
	  zoom: 17,
	  mapTypeId: google.maps.MapTypeId.ROADMAP,
	}
	var map = new google.maps.Map(mapCanvas, mapOptions)

	var marker = new google.maps.Marker({
	  position: new google.maps.LatLng(-37.848978, 144.989035),
	  map: map,
	  icon: '/assets/img/map-marker.png'
	});
}
function initLayout() {
	if( $(window).width() < 992 ) {
		//Make sure the last section is tall enough to execute menu
		$('.content section:last-child').css('height', $(window).height());

	} else {
		//otherwise close menu, and reset section height
		closeMenu();
		$('.content section:last-child').css('height', '')
	}
}

function openMenu(ob) {
	$('html, body').animate({
        scrollTop: $(ob).offset().top
    }, 500, function() {
    	$(ob).parent('header').addClass('open');
    	$('body').addClass('menu-open').bind('touchmove', function(e){e.preventDefault()});
    });
}

function closeMenu() {
	$('header.open').removeClass('open');
	$('body').removeClass('menu-open').unbind('touchmove');
}

$(document).ready(function(){
	
	initGoogleMap();
	initLayout();

	//Sticky header for mobile up to 992px width window
	$('header').scrollToFixed({
		maxWidth: 992
	});
	
	//redraw things after resize window
	$(window).resize(function() {
		initLayout();
	});


	// Smooth Scrolling
	$('a').click(function(){
	    $('html, body').animate({
	        scrollTop: $( $.attr(this, 'href') ).offset().top
	    }, 1000);
	    return false;
	});

	// bootstrap Scrollspy
	$('body').scrollspy({ target: '.nav-block' })

	
	//Open close menu
	var menuLock = 0;
	$('.navbar-toggle').click(function(){
		if (menuLock == 0) {
			openMenu(this);
			menuLock = 1;
		} else if (menuLock == 1) {
			closeMenu();
			menuLock = 0;
		}
	});

	//CLose menu when menu item is selected
	$('.nav-block a').click(function(){
		var ob = $(this).parents('.nav-block').prev();
		closeMenu(ob);
		cloneLock = 0;
	});

});  